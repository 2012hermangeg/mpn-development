# MPN - Initiation and Development

Code for inferring initiation and development of Myeloproliferative Neoplasms.



To execute the code, you need first to install the MarkovProcesses package from Mahmoud Bentriou available at:<br/>
https://gitlab-research.centralesupelec.fr/2017bentrioum/markovprocesses.jl

CALRm data come from:<br/>
El-Khoury, M. et al. Different impact of calreticulin mutations on human hematopoiesis in myeloproliferative neoplasms. Oncogene 39, 5323–5337 (2020).



JAK2V617F data come from:<br/>
M. Mosca, et al., Inferring the dynamic of mutated hematopoietic stem and progenitor cells induced by IFNα in myeloproliferative neoplasms. Blood, blood.2021010986 (2021).
