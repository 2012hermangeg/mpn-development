#################################
### Script Julia              ###
### Inferring initiation and  ###
### development of MPN        ###
#################################

@everywhere using DataFrames, CSV # To Load data
@everywhere using Distributions, Random
@everywhere using BenchmarkTools
@everywhere using Hungarian #For optimal assigment in ABC
@everywhere using MarkovProcesses # Statistical framework to perform ABC-SMC
# Available at:
# https://gitlab-research.centralesupelec.fr/2017bentrioum/markovprocesses.j


#We consider single observations from MPN patients (either CALRm or JAK2V617F) without homozygotes subclones
data = CSV.read("data_CALRm_El-Khoury.csv",DataFrame) #Here for example, CALRm population
#Dt_days are the age of the patients (in days) when their CF is measured
#CF_90pos_het is the Clonal Fractions of (heterozygous) mutated progenitor cells [in %].

#Vector of observations (so that CF between 0 and 1)
vec_observations = data[!,:CF_90pos_het]./100.0; 
sample_times = Array{Float64}(data[!,:Dt_days]) #Corresponding times

max_days = Float64(maximum(data[!,:Dt_days])) + 1
min_days = Float64(minimum(data[!,:Dt_days])) - 1

# We define the model as a Chemical Reaction Network
# We only focus on mutated HSCs
invasion = @network_model begin
    sym: (HSC => 2HSC, alpha*p2*HSC) #Symetrical divisions
    asym: (HSC => 1HSC + abs, alpha*(1.0-p2-p0)*HSC) #asymetrical divisions. NB: abs would correspond to a progenitor cell, but actually we do not model progenitor cells here
    diff: (HSC => 0, alpha*p0*HSC*(1.0+l-l+beta-beta)) #differentiated divisions and dummy way to introduce other parameters
    end "Process for mutated stem cells"


#The clonal expansion starts with one mutated HSC
set_x0!(invasion, [1, 0]);
#Trajectories will not be computed beyond max_days
set_time_bound!(invasion, max_days)


### Deterministic approximation ###
# We switch to the deterministic approximation when we reach 2000 mutated HSCs
@everywhere stop_val = 2_000
@everywhere my_criteria(p, x) = x[1] >= stop_val
change_simulation_stop_criteria(invasion, :my_criteria)
@everywhere model_det(t,t0,N0,Delta,alpha) =  min.(N0.*exp.(alpha*Delta.*(t.-t0)), 1e300) # Determinitic law


### Definition of a hybrid model model ###
@everywhere mutable struct HybridInvasionModel <: Model
    network_model::ContinuousTimeModel
    sample_times::Vector{Float64}
    max_days::Float64
    min_days::Float64
    stop_val::Int
    N_WT::Float64
end

@everywhere import MarkovProcesses: get_proba_model
@everywhere get_proba_model(hybrid_model::HybridInvasionModel) = hybrid_model.network_model

@everywhere struct HybridTrajectory <: AbstractTrajectory
    sim_vector::Vector{Float64} #Vector of the simulated number of mutated HSCs
    CF_vector::Vector{Float64} #Vector of CF among progenitor cells
end

@everywhere function simulate(hybrid_model::HybridInvasionModel; 
                              p::Union{Nothing,AbstractVector{Float64}} = nothing)
    

    ## Choose the parameter vector for simulation
    p_sim = (hybrid_model.network_model).p
    if p != nothing
        p_sim = p
    end

    ## Simulation of the stochastic process
    n_obs = length(hybrid_model.sample_times)
    stochastic_sim = simulate(hybrid_model.network_model; p = p_sim)
    while stochastic_sim.values[1][end] == 0
        # We simulate only trajectories without extinction
        stochastic_sim = simulate(hybrid_model.network_model; p = p_sim)
    end

    #At which age the stochastic process stop?
    N_end = stochastic_sim.values[1][end] #HSC final
    T_simu = N_end == hybrid_model.stop_val ? stochastic_sim.times[end-1] : hybrid_model.max_days

    param_to_idx = hybrid_model.network_model.map_param_idx
    p_l = p_sim[param_to_idx[:l]] #parameter l
    fetal_life = p_sim[param_to_idx[:beta]] < 0.5 ? true : false
    #fetal_life equal to true corresponds to the hypothesis of mutation  acquisition in fetal life
    # beta is the parameter included in the parameter estimation procedure that allows to make the distinction between the 2 hypotheses we study
 
    T_apparition = fetal_life ? 0.0 : rand(Exponential(p_l)) #Corresponds to T0

    sple_times = max.(0.0,hybrid_model.sample_times .- T_apparition) #Takes into account the fact that the mutation is acquired at time T0 (or T_apparition)


    hybrid_sim = zeros(n_obs)
    ind_stoch = sple_times .<= T_simu
    hybrid_sim[ind_stoch] = vectorize(stochastic_sim, :HSC, sple_times)[ind_stoch]

    ### Switch to the deterministic approximation ###
    ind_det = sple_times .> T_simu

    p_alpha = p_sim[param_to_idx[:alpha]]
    p_p2 = p_sim[param_to_idx[:p2]]
    p_p0 = p_sim[param_to_idx[:p0]]
    p_Delta = p_p2 - p_p0

    hybrid_sim[ind_det] = model_det(sple_times,T_simu,N_end,p_Delta, p_alpha)[ind_det] #Deterministic law
   
    #Compute CF among progenitor cells
    p_N_WT = hybrid_model.N_WT #Number of WT HSCs
    A = p_alpha*(1.0-p_Delta)
    CF_vector = A.*hybrid_sim./(A.*hybrid_sim .+ p_alpha*p_N_WT) #CF among progenitor cells

    return HybridTrajectory(hybrid_sim, CF_vector)
end

N_WT = 100_000 #According to Lee-Six et al. Nature, 2018
hybrid_invasion = HybridInvasionModel(invasion, sample_times, max_days,min_days, stop_val, N_WT)



### Estimation ###

# Distance function for hybrid_invasion model
@everywhere function hybrid_dist_obs(vec_sim, vec_observations) 
    #vec_sim is a vector containing Np different trajectories, with Np the number of patients (or single observations) considered
    #vec_observations is the vector of our Np observations 

    n_sim = length(vec_sim)
    n_obs = length(vec_observations) #We must have n_sim = n_obs
    
    ### Optimal Assignment procedure ###
    CF_matrix = zeros(n_sim,n_obs)
    for i in 1:n_sim
        CF_matrix[i,:] = vec_sim[i].CF_vector
    end
    
    #Optimal assingment using the Hungarian Algorithm
    cost_matrix = (CF_matrix .- vec_observations').^2 #L2-norm
    
    assignment = hungarian(cost_matrix)[1]
    #assigment[i] gives the index of the observation corresponding to the ith simulation
    
    return mean_error = sum([cost_matrix[i,assignment[i]] for i =1:n_obs])/n_obs
end


#Custom joint prior distribution for our parameters
@everywhere struct PriorDistribution <: ContinuousMultivariateDistribution end

@everywhere function Distributions.length(d::PriorDistribution)
    return 5 #Number of parameters
end

#prior law for parameter alpha
@everywhere alpha_gamma = 1/30 #prior mean value for alpha
@everywhere COV = 0.1 #Coefficient of variation
@everywhere k = 1/COV^2
@everywhere theta = alpha_gamma * COV^2

@everywhere max_extinction = 1.0 #Maximum probability of extinction we allow

@everywhere function Distributions.rand(d::PriorDistribution)
    a = rand(Gamma(k, theta)) #alpha
    y = rand(Uniform(0,0.5)) #p0
    x = rand(Uniform(0,1)) #p2
    while x+y > 1 || y >= x
      y = rand(Uniform(0,0.5)) #p0
      x = rand(Uniform(0,1)) #p2
    end
    l = rand(Uniform(0,100.0*365)) #l
    beta_continous = rand(Uniform(0,1)) #To determine whether hyp. T0 = 0 or T0>0 is considered, according to the value wrt 0.5
    #beta_continuous < 0.5 means beta = 0 (T0=0), otherwise beta = 1 (T0>0)
    return [a,x,y,l,beta_continous]
end

@everywhere function Distributions.rand!(d::PriorDistribution,vec_p::Array{Float64,1})
    vec_p[:] = rand(d)
end

@everywhere function Distributions.pdf(d::PriorDistribution, X::AbstractArray{T,1} where T=5) 
    (a,x,y,l,beta_continous) = X
    if 0.0 <= x <= 1.0
        if 0.0 <= y <= min(max_extinction*x,1.0-x)
            return 0.25*pdf(Gamma(k, theta),a)* pdf(Uniform(0,100.0*365),l)*pdf(Uniform(0,1),beta_continous)
        else
            return 0.0
        end
    else
        return 0.0
    end
end

@everywhere function Distributions.insupport(d::PriorDistribution, x::Array{Float64,1}) 
    return pdf(d,x) != 0 # in support if non zero
end

parametric_model = ParametricModel(hybrid_invasion, [:alpha,:p2,:p0, :l, :beta], PriorDistribution())


# First arbitrary values
alpha = 1.0/30.0
p2 = 0.03
p0 = 0.01
l = 10.0*365 #days
beta_continous = 0.6
#To have an idea of the error value
set_param!(hybrid_invasion, [alpha,p2,p0, l,beta_continous]);
Random.seed!(12)
@time epsilon = hybrid_dist_obs([simulate(hybrid_invasion) for i=1:length(vec_observations)], vec_observations)
@show epsilon

###########################
### SMC-ABC Computation ###
###########################

Random.seed!(90)
res_abc = abc_smc(parametric_model, vec_observations, 
                  hybrid_dist_obs, nbr_particles = 2_000, tolerance = 0.002, alpha = 0.5, save_iterations = true, 
                  dir_results="/results")





